# A repository of Michelson contracts and static analysis results over them

---

Developed under the [FRESCO](https://release.di.ubi.pt/projects/fresco.html)
project (Formal Verification of Smart Contracts), generously funded by [Tezos
Foundation](https://tezos.foundation).
